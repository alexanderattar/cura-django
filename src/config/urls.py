from django.conf.urls import patterns, include, url
from django.conf.urls.defaults import *
from django.contrib import admin
from django.views.generic import RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from images.views import ImageGridView, ImageFeedView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^search/$', 'images.views.home', name='search'),
    url(r'^images/$','images.views.images',name='images'),
    url(r'^about/$','images.views.about',name='about'),
    url(r"^$", ImageGridView.as_view(), name="grid"),
    url(r"^feed/$", ImageFeedView.as_view(), name="feed"),

    # admin
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
