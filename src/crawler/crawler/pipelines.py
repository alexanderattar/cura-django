import logging
from images.models import Image
from images.utils import get_image_size

from scrapy.exceptions import DropItem

logger = logging.getLogger(__name__)


class ImagePipeline(object):

    def process_item(self, item, spider):

        # check for duplicates
        url_count = Image.objects.filter(url=item['url']).count()

        # if it doesn't already exist, save the item
        if url_count > 0:
            raise DropItem("Duplicate item found in database: %s" % item)
        else:
            try:  # to get image info
                file_size, width, height = get_image_size(item['url'])
                logger.info('Image: %s, Width: %s, Height: %s' % (item['url'], width, height))
            except Exception as e:
                logger.error(str(e))

            if width < 300:  # filter out small images
                raise DropItem("Image is too small: %s" % item)
            else:
                item.save()
            return item
