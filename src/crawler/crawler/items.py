from images.models import Image
from scrapy.contrib.djangoitem import DjangoItem


class ImageItem(DjangoItem):
    django_model = Image
