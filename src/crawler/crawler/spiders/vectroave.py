from image_spider import ImageSpider

class VectroaveSpider(ImageSpider):
    name = "vectroave"
    allowed_domains = ["http://vectroave.com/"]
    start_urls = [
        "http://vectroave.com/"
    ]