from image_spider import ImageSpider

class PlanetDamageSpider(ImageSpider):
    name = "planetdamage"
    allowed_domains = ["http://planetdamage.tumblr.com/"]
    start_urls = [
        "http://planetdamage.tumblr.com/?page=%s" % page for page in xrange(0, 100)
    ]