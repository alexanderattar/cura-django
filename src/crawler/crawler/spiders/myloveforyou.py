from image_spider import ImageSpider

class MyLoveForYouSpider(ImageSpider):
    name = "myloveforyou"
    allowed_domains = ["http://myloveforyou.typepad.com/"]
    start_urls = [
        "http://myloveforyou.typepad.com/my_love_for_you/page/%s/" % page for page in xrange(0, 100)
    ]