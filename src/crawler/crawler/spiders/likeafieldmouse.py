from image_spider import ImageSpider

class LikeAFieldMouseSpider(ImageSpider):
    name = "likeafieldmouse"
    allowed_domains = ["http://likeafieldmouse.com/"]
    start_urls = [
        "http://likeafieldmouse.com/?page=%s" % page for page in xrange(0, 100)
    ]