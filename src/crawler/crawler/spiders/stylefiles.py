from image_spider import ImageSpider

class StyleFilesSpider(ImageSpider):
    name = "stylefiles"
    allowed_domains = ["http://style-files.com/"]
    start_urls = [
        "http://style-files.com/"
    ]