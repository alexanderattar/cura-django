from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector

from crawler.items import ImageItem


class ImageSpider(BaseSpider):

    def parse(self, response):
        items = []
        hxs = HtmlXPathSelector(response)
        images = hxs.select('//img/@src')
        for image in images:
            item = ImageItem()
            item['url'] = image.extract()
            item['name'] = image.extract().split('/')[-1]
            items.append(item)
        return items


