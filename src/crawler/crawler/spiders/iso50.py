from image_spider import ImageSpider

class Iso50Spider(ImageSpider):
    name = "iso50"
    allowed_domains = ["http://www.iso50.com/"]
    start_urls = [
        "http://www.iso50.com/?page=%s" % page for page in xrange(0, 100)
    ]