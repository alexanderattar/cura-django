from image_spider import ImageSpider

class BeautifulDecaySpider(ImageSpider):
    name = "beautifuldecay"
    allowed_domains = ["http://beautifuldecay.com/"]
    start_urls = [
        "http://beautifuldecay.com/"
    ]