from image_spider import ImageSpider

class LongingForSeoulSpider(ImageSpider):
    name = "longingforseoul"
    allowed_domains = ["http://longing-for-seoul.tumblr.com/"]
    start_urls = [
        "http://longing-for-seoul.tumblr.com/?page=%s" % page for page in xrange(0, 50)
    ]