from image_spider import ImageSpider

class ALittleRestSpider(ImageSpider):
    name = "alittlerest"
    allowed_domains = ["http://www.alittlerest.com/"]
    start_urls = [
        "http://www.alittlerest.com/"
    ]