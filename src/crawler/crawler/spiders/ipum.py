from image_spider import ImageSpider

class IpumSpider(ImageSpider):
    name = "ipum"
    allowed_domains = ["http://ipu-m.tumblr.com/"]
    start_urls = [
        "http://ipu-m.tumblr.com/?page=%s" % page for page in xrange(0, 50)
    ]