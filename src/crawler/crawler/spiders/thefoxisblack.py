from image_spider import ImageSpider

class TheFoxIsBlackSpider(ImageSpider):
    name = "thefoxisblack"
    allowed_domains = ["http://www.thefoxisblack.com/"]
    start_urls = [
        "http://www.thefoxisblack.com/"
    ]