from image_spider import ImageSpider

class StormTrooperFashionSpider(ImageSpider):
    name = "stormtrooperfashion"
    allowed_domains = ["http://stormtrooperfashion.com/"]
    start_urls = [
        "http://stormtrooperfashion.com/?page=%s" % page for page in xrange(0, 50)
    ]