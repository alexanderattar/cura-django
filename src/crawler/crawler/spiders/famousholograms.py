from image_spider import ImageSpider

class FamousHologramsSpider(ImageSpider):
    name = "famousholograms"
    allowed_domains = ["http://famousholograms.tumblr.com/"]
    start_urls = [
        "http://famousholograms.tumblr.com/?page=%s" % page for page in xrange(0, 100)
    ]