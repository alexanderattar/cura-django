import os
import sys
import imp
from os.path import join, abspath, dirname
from django.core.management import setup_environ

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'crawler (+http://www.yourdomain.com)'

COMMANDS_MODULE = 'crawler.commands'

BOT_NAME = 'crawler'
SPIDER_MODULES = ['crawler.spiders']
NEWSPIDER_MODULE = 'crawler.spiders'

# Path configuration
here = lambda *x: join(abspath(dirname(__file__)), *x)
PROJECT_ROOT = here('..', '..')  # src dir
root = lambda *x: join(abspath(PROJECT_ROOT), *x)  # src dir


# Django
def setup_django_env(path):
    f, filename, desc = imp.find_module('settings', [path])
    project = imp.load_module('settings', f, filename, desc)

    setup_environ(project)

    # Add path's parent directory to sys.path
    sys.path.append(os.path.abspath(os.path.join(path, os.path.pardir)))

# pass in the config directory path
setup_django_env(root('config'))


# For processing our items
ITEM_PIPELINES = [
    'crawler.pipelines.ImagePipeline',
]
