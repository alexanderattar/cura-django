from django.contrib import admin
from images.models import Image, Source, Tag

class ImageAdmin(admin.ModelAdmin):
    search_fields = ('name', 'url')
    list_display = ('name', 'url')

admin.site.register(Image, ImageAdmin)
admin.site.register(Tag)
admin.site.register(Source)
