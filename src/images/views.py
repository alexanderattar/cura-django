import logging

from django.views.generic import ListView
from django.shortcuts import render_to_response
from decorators import template
from django.core.context_processors import csrf

from pure_pagination.mixins import PaginationMixin

from models import Image

from imagegrabber import main

logger = logging.getLogger(__name__)


class ImageGridView(PaginationMixin, ListView):
    queryset = Image.objects.all().order_by('?')  # randomize queryset
    paginate_by = 7

@template('home.html')
def home(request):
    pass

@template('about.html')
def about(request):
    pass

@template('viewer.html')
def images(request):
    """ image viewer """

    if request.method == 'POST':
        logger.info('URL input: %s' % request.POST.get('url'))

        if request.POST.get('previous_page'):
            logger.info('Previous page: %s' % request.POST.get('previous_page'))

        data = main(request.POST.get('url'))

        if not data:
            return 'There was an error :('

        if 'next_page' in data:
            logger.info('Next page: %s' % data['next_page'])

        return {
            'images': data['images'],
            'previous_page': data['previous_page'],
            'next_page': data['next_page']
        }

class ImageFeedView(PaginationMixin, ListView):
    queryset = Image.objects.all().order_by('?')  # randomize queryset
    template_name = 'images/feed.html'
    paginate_by = 7


