from django.db import models


class Image(models.Model):
    name = models.CharField(max_length=255, blank=True)
    url = models.CharField(max_length=255, unique=True, blank=True)
    tags = models.ManyToManyField('images.Tag')
    sources = models.ManyToManyField('images.Source')

    def __unicode__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.name

class Source(models.Model):
    name = models.CharField(max_length=255, blank=True)
    url = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.name