import time
import random
import logging
import urllib2

import ImageFile
from bs4 import BeautifulSoup as bs

logger = logging.getLogger(__name__)

def soupify(url):
    """ Take a url and make beautiful soup out of it """

    # handling for urls that have been entered without the http
    if not url.lower().startswith('http'):
        url = 'http://' + url
    request = urllib2.Request(url, None, randomize_user_agent())

    try:  # to open the page
        html = urllib2.urlopen(request)
        soup = bs(html)
    except Exception as e:
        soup = None
        logger.error('Unable to open page. exception=%s' % str(e))
    return soup

def get_image_size(url):
    """ get file size *and* image size (None if not known) """

    stime = time.time()
    response = urllib2.urlopen(url)
    size = response.headers.get("content-length")

    logger.info('elapsed_time=%sms' % round((time.time() - stime) * 100, 2))

    if size:
        size = int(size)
    p = ImageFile.Parser()
    while 1:
        data = response.read(24)
        if not data:
            break
        p.feed(data)
        if p.image:
            return size, p.image.size[0], p.image.size[1]
            break
    response.close()
    return size, None, None


def randomize_user_agent():
    """
    Sets up request headers with a randomized User-Agent
    """

    user_agents = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1309.0 Safari/537.17',
        'Mozilla/6.0 (Windows NT 6.2; WOW64; rv:16.0.1) Gecko/20121011 Firefox/16.0.1',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2',
        'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)',
        'Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))',
        'Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; GTB7.4; InfoPath.2; SV1; .NET CLR 3.3.69573; WOW64; en-US)'
    ]

    return {'User-Agent': random.choice(user_agents)}
