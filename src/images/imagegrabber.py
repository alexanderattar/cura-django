import re
import sys
import logging

from urllib       import urlretrieve
from urlparse     import urlparse
from os.path      import expanduser

from utils        import soupify, get_image_size

logger = logging.getLogger(__name__)

ANALYZE_SIZE = False  # flag to enable/disable image size analysis


def main(url):

    # TODO - build out logic to cache images
    # Note - probably want to store the urls and image file names

    soup = soupify(url)
    blog_type = detect_blog_type(url, soup)
    logger.info(url)

    return {'images': get_images(soup), 'previous_page': url, 'next_page': find_next_page(url, soup, blog_type)}


def detect_blog_type(url, soup):

    blog_type = None

    if soup:
        if (re.search('blogspot', url) or
           re.search('blogger', url) or
           len(soup.findAll(text=re.compile('blogspot'))) > 0):
            blog_type = 'blogspot'
        if re.search('tumblr', url) or re.search('tumblr', url):
            blog_type = 'tumblr'

        logger.info('Blog type is %s' % blog_type)

    return blog_type


def find_next_page(url, soup, blog_type):

    next_page = None
    next_link = None
    previous_link = None
    supported_blogs = ['blogspot', 'tumblr']
    base_url = '%s://%s' % (urlparse(url).scheme, urlparse(url).netloc)
    url_path = urlparse(url).path
    logger.info('Base URL: %s, Path: %s' % (base_url, url_path))

    try:  # to get the link to the next page
        if blog_type in supported_blogs:

            if blog_type == 'blogspot':
                next_page = soup.find("a", {"class": "blog-pager-older-link"})['href']

            if blog_type == 'tumblr':
                if not re.search('page', url_path):
                    next_page_number = 2
                else:
                    next_page_number = int(url_path[-1]) + 1

                next_page = base_url + '/page/%s' % next_page_number

            else:
                logger.info('Blog type is unable to be detected by the url')

        else:  # the blog type is unable to be detected by the url
            # check if new pages exist following the <url>/page/<page_number> paradigm
            if not re.search('page', url_path):
                next_page_number = 2
            else:
                next_page_number = int(url_path[-1]) + 1

            try:  # to see if the next page exists at the base url plus page number
                next_page = base_url + '/page/%s' % next_page_number
                if soupify(next_page):
                    return next_page
            except Exception as e:
                logger.error('A new page doesn\'t exist at this link. exception=%s' % str(e))

            try:  # to see if the next page exists at the base url plus the path and number
                next_page = base_url + url_path + '/page/%s' % next_page_number
                if soupify(next_page):
                    return next_page
            except Exception as e:
                logger.error('A new page doesn\'t exist at this link. exception=%s' % str(e))

            # search for next page links based on keywords such as next previous ect.
            if soup.find('a', text='previous'):
                previous_link = soup.find('a', text='previous')['href']

            if soup.find('a', text='next'):
                next_link = soup.find('a', text='next')['href']

            # How do we figure out which link is the right link?
            # This isn't going to work if both links are there
            if previous_link and not next_link:
                next_page = previous_link
            elif next_link and not previous_link:
                next_page = next_link

            elif not next_page:
                logger.info('Unable to find next page link.')

    except Exception as e:
        next_page = None
        logger.error('Unable to find next page link. exception=%s' % str(e))

    return next_page


def get_images(soup):
    """ stores images in a lookup table """

    images = {}

    if soup:

        for image in soup.findAll('img'):
            if ANALYZE_SIZE:
                try:  # to get image info
                    file_size, width, height = get_image_size(image['src'])
                    logger.info('Image: %s, Width: %s, Height: %s' % (image['src'], width, height))
                except Exception as e:
                    logger.error(str(e))

            try:  # to get the image
                if not ANALYZE_SIZE or width >= 300:  # filter out small images - TODO - gotta make this faster
                    filename = image['src'].split('/')[-1]
                    images[filename] = image['src']
            except Exception as e:
                logger.error('Unable to get image: %s' % str(e))

    return images


def download_images(soup):

    # TODO - Refactor to download only favorited images
    home = expanduser("~")  # setup a path to the OSX ~/ directory
    download_directory = home + '/Downloads/'

    for image in soup.findAll('img'):

        try:  # to save the image
            logger.info('Image: %(src)s' % image)
            filename = image['src'].split('/')[-1]
            file_location = download_directory + filename

            if image['src'].lower().startswith('http'):
                urlretrieve(image['src'], file_location)
        except Exception as e:
            logger.error('Unable to save image: %s' % str(e))

if __name__ == '__main__':
    url = sys.argv[-1]
    main(url)
