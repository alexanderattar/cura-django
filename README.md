Cura is a content curation and discovery platform.

The current focus is visual media, but the underlying technology is powerful, 
providing the potential for various other types of content aggregation. 
The application works by interacting with other websites, while gathering data 
that has been chosen and defined within the codebase. 
There is some basic filtering to ensure that the images are of high quality, 
before being added to the database. In addition to this predefined filter, 
I am also working to develop an variety of sorting options that will be 
available to the user. Some ideas include content type (image, gif, sound ect.),
primary color (if visual), size, and tags.